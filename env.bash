export REGION=europe-west1
export PROJECT_ID=hyperd-containers
export PROJECT_NAME=hyperd-containers
export GOOGLE_APPLICATION_CREDENTIALS=$PWD/secrets/gcloud/key.json
export REGISTRY="eu.gcr.io"
export BASE_DIR=$(pwd)
