# NGiNX Config

[![pipeline status](https://gitlab.com/hyperd/ansible-role-nginx-config/badges/master/pipeline.svg)](https://gitlab.com/hyperd/ansible-role-nginx-config/-/commits/master)

Ansible Role to provision and configure NGiNX on RHEL based systems.

## Requirements

WIP

## Role Variables

```yaml
---
# defaults file for nginx-config
# Used only for RHEL based installation, enables source Nginx repo.
nginx_yum_repo_enabled: true

# The name of the nginx package to install.
nginx_package_name: "nginx"

nginx_service_state: started
nginx_service_enabled: true

nginx_conf_template: "nginx.conf.j2"
nginx_vhost_template: "vhost.j2"

nginx_worker_processes: >-
  "{{ ansible_processor_vcpus | default(ansible_processor_count) }}"
nginx_worker_connections: "8096"
nginx_multi_accept: "on"
nginx_use_epoll: true

nginx_error_log: "/var/log/nginx/error.log warn"
nginx_access_log: "/var/log/nginx/access.log main buffer=16k flush=2m"

nginx_sendfile: "on"
nginx_tcp_nopush: "on"
nginx_tcp_nodelay: "on"

nginx_keepalive_timeout: "30"
nginx_keepalive_requests: "100000"

nginx_server_tokens: "off"

nginx_client_max_body_size: "5M"

nginx_server_names_hash_bucket_size: "64"

nginx_proxy_cache_path: ""

nginx_extra_conf_options: |
  # worker_rlimit_nofile: Number of file descriptors used for nginx.
  # The limit for the maximum FDs on the server is usually set by the OS.
  # If you don't set FD's then OS settings will be used which is by default 2000
  worker_rlimit_nofile 40000;

nginx_extra_http_options: |
    map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
    }

    # NOTE: You should only run with this enabled if you're not editing the files at the time you're serving them.
    # Because file accesses are cached any 404s will be cached too, similarly file-sizes will be cached,
    # and if you change them your served content will be out of date.

    # cache informations about FDs, frequently accessed files
    # can boost performance, but you need to test those values
    open_file_cache max=200000 inactive=20s;
    open_file_cache_valid 60s;
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    # allow the server to close connection on non responding client, this will free up memory
    reset_timedout_connection on;

    # request timed out -- default 60
    client_body_timeout 10;

    # if client stop responding, free up memory -- default 60
    send_timeout 2;

# Example extra http options, printed inside the main server http config:
#    nginx_extra_http_options: |
#      proxy_buffering    off;
#      proxy_set_header   X-Real-IP $remote_addr;
#      proxy_set_header   X-Scheme $scheme;
#      proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
#      proxy_set_header   Host $http_host;

nginx_enable_gzip: "on"
nginx_enable_gzip_static: "on"
nginx_gzip_compression_level: 4

nginx_remove_default_vhost: false
nginx_vhosts: []
# Example vhost below, showing all available options:
# - listen: "80" # default: "80"
#   server_name: "example.com" # default: N/A
#   root: "/var/www/example.com" # default: N/A
#   index: "index.html index.htm" # default: "index.html index.htm"
#   filename: "example.com.conf" # Can be used to set the vhost filename.
#
#   # Properties that are only added if defined:
#   server_name_redirect: "www.example.com" # default: N/A
#   error_page: ""
#   access_log: ""
#   error_log: ""
#   extra_parameters: "" # Can be used to add extra config blocks (multiline).
#   template: "" # Can be used to override the `nginx_vhost_template` per host.
#   state: "absent" # To remove the vhost configuration.

# Example of upstreams
# nginx_upstreams:
#   - name: uwsgi
#     servers: {
#       "127.0.0.1:8050"
#     }
#   - name: daphne
#     servers: {
#       "127.0.0.1:8051"
#     }
# - name: myapp1
#   strategy: "ip_hash" # "least_conn", etc.
#   keepalive: 16 # optional
#   servers: {
#     "srv1.example.com",
#     "srv2.example.com weight=3",
#     "srv3.example.com"
#   }

nginx_log_format: |-
  '$remote_addr - $remote_user [$time_local] "$request" '
  '$status $body_bytes_sent "$http_referer" '
  '"$http_user_agent" "$http_x_forwarded_for"'

```

## Example Playbook

```yaml
---
- name: Configure NGiNX
  hosts: nginx_servers
  gather_facts: true
  tasks:
    - name: Install required packages
      yum:
        name:
          - openssl
          - python
          - unzip
        state: present

    - name: Get pip installer
      get_url:
        url: https://bootstrap.pypa.io/get-pip.py
        dest: /tmp/get-pip.py

    - name: Install pip
      command: python /tmp/get-pip.py
      changed_when: false

    - name: Install python requirements
      command: pip install cryptography
      changed_when: false

    - name: Check if /etc/hyperd exists
      stat:
        path: /etc/hyperd/tls/csr
      register: etc_hyperd

    - name: Create /etc/hyperd/tls/csr if it does not exist
      file:
        path: "{{ item }}"
        state: directory
        mode: "0500"
        recurse: true
      loop:
        - /etc/hyperd/tls/csr
      when:
        - not etc_hyperd.stat.isdir is defined

    - name: Generate an OpenSSL private key with the default values (4096 bits, RSA)
      openssl_privatekey:
        path: /etc/hyperd/tls/hyperd.key
        select_crypto_backend: cryptography

    - name: Generate an OpenSSL Certificate Signing Request with Subject information
      openssl_csr:
        path: /etc/hyperd/tls/hyperd.sh.csr
        privatekey_path: /etc/hyperd/tls/hyperd.key
        country_name: NL
        organization_name: Hyperd
        email_address: hyper@hyperd.sh
        common_name: testing.hyperd.sh

    - name: Generate a Self Signed OpenSSL certificate
      openssl_certificate:
        path: /etc/hyperd/tls/hyperd.crt
        privatekey_path: /etc/hyperd/tls/hyperd.key
        csr_path: /etc/hyperd/tls/hyperd.sh.csr
        provider: selfsigned

    - name: Include nginx-config
      include_role:
        name: "ansible-role-nginx-config"
      vars:
        nginx_enable_gzip: "on"
        nginx_enable_gzip_static: "on"
        nginx_gzip_compression_level: 6

        nginx_remove_default_vhost: false
        nginx_vhosts:
          - listen: "443 default_server ssl http2"
            vhost_name: "main"
            server_name: "localhost"
            root: /usr/share/nginx/html
            index: index.html index.htm
            extra_parameters: |
              ssl_certificate /etc/hyperd/tls/hyperd.crt;
              ssl_certificate_key /etc/hyperd/tls/hyperd.key;
              ssl_session_timeout 1d;
              ssl_session_cache shared:SSL:10m;  # about 40000 sessions
              ssl_session_tickets off;

              # intermediate configuration
              ssl_protocols TLSv1.2;
              ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384; # noqa 204
              ssl_prefer_server_ciphers off;

              # HSTS (ngx_http_headers_module is required) (63072000 seconds)
              add_header Strict-Transport-Security "max-age=63072000" always;

              # Protect against click-jacking https://www.owasp.org/index.php/Testing_for_Clickjacking_(OTG-CLIENT-009)
              add_header X-Frame-Options "DENY";

              location ~* \.(jpg|jpeg|gif|png|css|js|ico|xml)$ {
                  root   /usr/share/nginx/html;
                  access_log        on;
                  log_not_found     on;
                  expires           30d;
              }
          - listen: "80 default_server"
            vhost_name: "default"
            server_name_redirect: $host:443
            server_name: "localhost"

```

## Test

The testing environment leverages [molecule](https://molecule.readthedocs.io/en/stable/index.html).
To run all the tests, after installing molecule e.g. `pip3 install molecule[docker]`, execute the following command in a terminal:

```bash
molecule test
```

Molecule will run **yamllint** and **ansible-lint** against the playbooks; it will provision a docker container based on CentOS with systemd enabled.
During the tests **NGiNX** will be installed and configured with two vhosts, listening on port 80 and 443/TLS.
The **verify** stage will check that the endpoints are responding correctly, and that all the headers will be sent to the client as expected:

```yaml
  # ...
  - name: Assert that the response headers are those expected
    assert:
      that:
        - tls_static_result.content_encoding == "gzip"
        - tls_result.x_frame_options == "DENY"
        - tls_static_result.cache_control is defined
        - tls_result.strict_transport_security is defined
```

## License

[GNU General Public License v3 (GPLv3)](./LICENSE)

## About the author

[Francesco Cosentino](https://www.linkedin.com/in/francesco-cosentino/)

I'm a surfer, a crypto trader, and a DevSecOps Engineer with 15 years of experience designing highly-available distributed production environments and developing cloud-native apps in public and private clouds.
