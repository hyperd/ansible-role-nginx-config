FROM centos/systemd

COPY ./.docker/sudo /usr/bin

RUN chmod +x /usr/bin/sudo

LABEL org.opencontainers.image.vendor="Hyperd" \
    org.opencontainers.image.url="eu.gcr.io/hyperd-containers/centos" \
    org.opencontainers.image.title="Base CentOS Docker Image" \
    org.opencontainers.image.description="A doceker image to build CentOS test containers." \
    org.opencontainers.image.version="v0.5" \
    org.opencontainers.image.documentation="README.md"
